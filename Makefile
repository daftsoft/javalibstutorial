JAVAC=$(JAVA_HOME)bin/javac
JAR=$(JAVA_HOME)bin/jar
PROJECT=HelloWorld.jar
MANIFEST=Manifest.txt
RESOURCES=Info.txt
# Имя нашей библиотеки, через которое в коде будет происходить доступ к 
# классам, реализованным в ней (package из файлов Hello.java и Bye.java):
PACKAGE_NAME=HelloPackage

# Имя .jar-файла с нашей библиотекой. Здесь это имя специально указано 
# отличное от внутреннего имени библиотеки, чтобы продемонстрировать 
# их независимость. Впоследствии это имя будет указано как classpath.
# Хотя, на практике, так делать скорее неправильно - от разницы
# имён файла библиотеки и её названия сильно пострадает читаемость и
# возможность дальнейшей поддержки кода.
LIBRARY_NAME=Hello.jar

all: $(PROJECT)

# Здесь мы компилируем код первого класса для библиотеки. Ключ -d
# создаёт библиотеку со всей структурой каталогов (создаёт эту 
# структуру если надо):
$(PACKAGE_NAME)/Hello.class: Hello.java
	@echo Compiling $^ \(package $(patsubst %/,%,$(dir $@))\):
	$(JAVAC) -d . $^

# Здесь мы компилируем и добавляем класс в каталог библиотеки.
# В одну библиотеку можно добавлять много классов, javac сам
# положит .class-файл в каталог, указанный в имени package.
$(PACKAGE_NAME)/Bye.class: Bye.java
	@echo Compiling $^ \(package $(patsubst %/,%,$(dir $@))\):
	$(JAVAC) -d . $^

# Компилируем приватный класс библиотеки:
$(PACKAGE_NAME)/ByeBye.class: Bye.java
	@echo Compiling $^ \(package $(patsubst %/,%,$(dir $@))\):
	$(JAVAC) -d . $^

# Здесь мы собираем .jar-файл с нашей библиотекой. В зависимостях 
# указываем пути к классам, которые мы хотим оформить в библиотеку:
$(LIBRARY_NAME): $(PACKAGE_NAME)/Hello.class \
		 $(PACKAGE_NAME)/Bye.class \
		 $(PACKAGE_NAME)/ByeBye.class
	@echo Building $@:
	$(JAR) -cvf $@ $^

# Компилируем класс высокого уровня (который пользуется библиотекой).
# Указываем в качестве classpath имя нашего .jar-файла.
HelloWorld.class: HelloWorld.java
	@echo Compiling $^:
	$(JAVAC) -cp $(LIBRARY_NAME) $^


# Здесь собираем всё в конечный .jar-файл. Используем маленький
# трюк - это правило и его зависимости составлены в таком
# порядке, в каком эти файлы должны быть переданы в параметры 
# jar, а в самой команде используются переменные Makefile - 
# сначала название самого правила ($@), являющееся именем 
# итогового .jar-файла, затем переменная, содержащая зависимости 
# ($^), состоящая из имени файла манифеста, с последующим 
# перечислением всех файлов, которые надо включить в архив:
$(PROJECT): $(MANIFEST) $(LIBRARY_NAME) HelloWorld.class $(RESOURCES)
	@echo Building $@:
	$(JAR) -cvfm $@ $^

clean:
	@rm -rf ./*.class $(PACKAGE_NAME) $(LIBRARY_NAME) $(PROJECT)

