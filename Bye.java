// Это файл, содержащий код второго из двух классов, которые мы 
// оформляем в библиотеки HelloPackage. Стоит помнить, что упаковывать 
// несколько публичных (экспортируемых) классов в одну библиотеку 
// неправильно с точки зрения архитектуры. Но делать это нужно, если 
// вы поставляете (экспортируете) класс, который использует другие 
// классы - для запаковки таких конструкций и придуманы библиотеки.
package HelloPackage;

// Приватный класс библиотеки, в пределах библиотеки он будет 
// доступен, а снаружи её к нему доступа не будет:
class ByeBye
{
  public void Print ()
  {
      System.out.println (getClass().getName() + ".Print ()");
  }
}

public class Bye
{
  public static void main (String args[]) throws Exception
  {
    // Демонстрация возможности упаковки исполняемых классов
    // в библиотеку, скомпилировав проект, попробуйте выполнить:
    // $JAVA_HOME/bin/java HelloPackage.Bye
    System.out.println ("Bye.main()");
  }
  public void Print ()
  {
      // Здесь класс ByeBye доступен:
      ByeBye BB = new ByeBye ();
      BB.Print();
      System.out.println (getClass().getName() + ".Print ()");
  }
}
