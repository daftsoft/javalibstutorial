// Импортируем нашу библиотеку. По сути дела import позволяет лишь
// сократить обращение к классу в последующем коде. Можно и без него
// обращаться к описанию класса Hello через HelloPackage.Hello, а к 
// классу Bye через HelloPackage.Bye.
import HelloPackage.*;
import java.io.*;

class HelloWorld
{
  public static void main (String args[]) throws Exception
  {
    // Вот так выглядело бы обращение к библиотечному классу
    // без import'а (тоже работает, но писать больше):
    // HelloPackage.Hello lHello = new HelloPackage.Hello ();
    // А так без import'а не сработает:
    Hello lHello = new Hello ();
    Bye lBye = new Bye ();
    
    // Ошибка: ByeBye является приватным классом библиотеки:
    // ByeBye lByeBye = new ByeBye ();
    
    // Получаем доступ к ресурсу - нашему текстовому файлу:
    InputStream lInputStream = HelloWorld.class.getResourceAsStream("Info.txt"); 
    BufferedReader lBufferedReader = 
                   new BufferedReader (new InputStreamReader(lInputStream)); 
    String lString = "";
    // Обращаемся к первому классу из нашей библиотеки:
    lHello.Print();
    // Выводим содержимое текстового файла из ресурсов:
    System.out.println ("Strings read from JAR-resource: ");
    while ((lString = lBufferedReader.readLine()) != null) 
      System.out.println(lString); 
    // Обращаемся к другому классу из нашей библиотеки:
    lBye.Print();
  }
}
