// Это файл, содержащий один из двух классов, которые мы оформим в 
// виде библиотеки. package указывает название библиотеки, по сути 
// дела каталог, в котором будут .class-файлы, содержащие 
// скомпилированные классы из всех файлов, в которых указано 
// название этой библиотеки. Чтобы продемонстрировать независимость 
// названий библиотеки и имён классов реализованных в ней, здесь 
// я специально сделал имя класса и имя библиотеки различными.
package HelloPackage;

public class Hello
{
  public void Print ()
  {
      System.out.println (getClass().getName() + ".Print ()");
  }
}
